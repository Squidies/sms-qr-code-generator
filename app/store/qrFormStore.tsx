import { create } from "zustand";

interface QrFormState {
  phone: string;
  isValidPhoneNumber: null | boolean;
  smsMessage: string;
  qrText: null | string;

  setPhone: (value: string) => void;
  setIsValidPhoneNumber: (value: null | boolean) => void;
  setSmsMessage: (value: string) => void;
  setQrText: (value: string) => void;
}

const useQrFormStore = create<QrFormState>()((set) => ({
  phone: "",
  isValidPhoneNumber: null,
  smsMessage: "",
  qrText: null,

  setPhone: (value: string) => set({ phone: value }),
  setIsValidPhoneNumber: (value: null | boolean) =>
    set({ isValidPhoneNumber: value }),
  setSmsMessage: (value: string) => set({ smsMessage: value }),
  setQrText: (value: string) => set({ qrText: value }),
}));

export default useQrFormStore;
