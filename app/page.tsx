"use client";

import LayoutNavigation from "./components/layout/LayoutNavigation";
import LayoutQrApp from "./components/layout/LayoutQrApp";

export default function Home() {
  return (
    <div className="h-screen">
      <div className="h-full">
        <LayoutNavigation />
        <LayoutQrApp />
      </div>
    </div>
  );
}
