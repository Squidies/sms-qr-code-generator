import dynamic from "next/dynamic";

const ThemeToggleInput = dynamic(
  () => import("../theme/themeToggle/ThemeToggleInput"),
  { ssr: false }
);

export default function LayoutNavigation() {
  return (
    <div className="border-b">
      <div className="container max-w-3xl m-auto p-2">
        <nav className="flex">
          <h1 className="text-2xl">SMS QR Code Generator</h1>
          <ThemeToggleInput className="ml-auto" />
        </nav>
      </div>
    </div>
  );
}
