import useQrFormStore from "@/app/store/qrFormStore";
import { useState } from "react";
import { QRCode } from "react-qrcode-logo";

export default function LayoutQrApp() {
  const qrForm = useQrFormStore();

  // (default) false == PNG // true == SVG
  const [pngOrSvg, setPngOrSvg] = useState(false);

  function handlePhoneInput(event: { target: HTMLInputElement }) {
    const input = event.target.value;
    const containsAlpha = /[a-zA-Z]/.test(input);
    const digits = input.replace(/\D/g, "");

    if (digits.length > 10 || containsAlpha) {
      qrForm.setIsValidPhoneNumber(false);
    } else if (digits.length < 10) {
      qrForm.setIsValidPhoneNumber(null);
    } else if (digits.length === 10) {
      qrForm.setIsValidPhoneNumber(true);
    }

    qrForm.setPhone(input);
  }

  function handleSmsInput(event: { target: HTMLTextAreaElement }) {
    qrForm.setSmsMessage(event.target.value);
  }

  function generateQrCode() {
    const encodedPhone = encodeURIComponent(`+1${qrForm.phone}`);
    const encodedMessage = encodeURIComponent(qrForm.smsMessage);

    qrForm.setQrText(`sms:${encodedPhone}&body=${encodedMessage}`);
  }

  function saveSvg() {
    const qrCodeCanvas = document.getElementById(
      "react-qrcode-logo"
    ) as HTMLCanvasElement;

    // Create a new SVG element
    const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
    svg.setAttribute("width", qrCodeCanvas.width.toString());
    svg.setAttribute("height", qrCodeCanvas.height.toString());

    // Create a new image element with the canvas data
    const image = document.createElementNS(
      "http://www.w3.org/2000/svg",
      "image"
    );
    image.setAttributeNS(
      "http://www.w3.org/1999/xlink",
      "xlink:href",
      qrCodeCanvas.toDataURL()
    );
    image.setAttribute("width", qrCodeCanvas.width.toString());
    image.setAttribute("height", qrCodeCanvas.height.toString());
    svg.appendChild(image);

    // Convert the SVG element to a string
    const svgString = new XMLSerializer().serializeToString(svg);

    // Create a Blob from the SVG string
    const blob = new Blob([svgString], { type: "image/svg+xml" });

    // Create a download link
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);
    link.download = "qrcode.svg";
    link.click();
  }

  function savePng() {
    const qrCodeCanvas = document.getElementById(
      "react-qrcode-logo"
    ) as HTMLCanvasElement;
    const link = document.createElement("a");
    link.href = qrCodeCanvas.toDataURL();
    link.download = "qrcode.png";
    link.click();
  }

  function saveQrCode() {
    // (default) false == PNG // true == SVG
    pngOrSvg ? saveSvg() : savePng();
  }

  return (
    <div className="md:flex container max-w-3xl m-auto">
      <section className="mt-8 mb-8 m-2 w-80">
        <form
          onSubmit={(event) => event.preventDefault()}
          className="flex flex-col max-w-sm"
        >
          <label htmlFor="phone">
            Phone # <span className="text-xs">{`(*required)`}</span>
          </label>
          <input
            name="phone"
            className="input input-bordered mb-2"
            type="tel"
            onChange={handlePhoneInput}
            value={qrForm.phone}
          />
          {qrForm.isValidPhoneNumber === false && (
            <p className="text-error text-sm">
              Please enter a valid 10-digit telephone number.
            </p>
          )}
          <label htmlFor="sms-message">
            Message <span className="text-xs">{`(*optional)`}</span>
          </label>
          <textarea
            className="textarea textarea-bordered mb-2"
            onChange={handleSmsInput}
            value={qrForm.smsMessage}
          />
          <button
            className="btn btn-primary mb-2"
            disabled={!qrForm.isValidPhoneNumber}
            onClick={generateQrCode}
          >
            Generate QR Code
          </button>
        </form>
      </section>
      <section className="w-full flex-1 m-2">
        <div className="border rounded-btn p-2 flex flex-col items-center justify-center min-h-80">
          {qrForm.qrText ? (
            <>
              <div className="qr-code-wrapper mb-2">
                <QRCode value={qrForm.qrText} size={280} />
              </div>
              <div className="flex flex-row-reverse">
                <div className="flex between items-center p-2">
                  <span className="px-1 inline-block text-xs">PNG</span>
                  <input
                    type="checkbox"
                    name="image-type"
                    className="toggle"
                    defaultChecked={pngOrSvg}
                    onClick={() => setPngOrSvg(!pngOrSvg)}
                  />
                  <span className="px-1 inline-block text-xs">SVG</span>
                </div>

                <button className="btn btn-primary" onClick={saveQrCode}>
                  SAVE QR CODE
                </button>
              </div>
            </>
          ) : (
            <div>Generate QR CODE</div>
          )}
        </div>
      </section>
    </div>
  );
}
