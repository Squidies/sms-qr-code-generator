import { useEffect, useState } from "react";
import ThemeToggleSunIco from "./ThemeToggleSunIco";
import ThemeToggleMoonIco from "./ThemeToggleMoonIco";

export default function ThemeToggleInput2({
  className,
}: {
  className: string;
}) {
  const localTheme = localStorage.getItem("prefers-dark-theme");
  let prefersDarkTheme = null;

  if (localTheme !== null) {
    prefersDarkTheme = JSON.parse(localTheme);
  } else {
    prefersDarkTheme = prefersDarkColorScheme();
  }

  const [darkMode, setDarkMode] = useState<boolean | null>(
    prefersDarkTheme || null
  );

  function prefersDarkColorScheme() {
    return window.matchMedia("(prefers-color-scheme: dark)").matches;
  }

  function toggleTheme() {
    setDarkMode(!darkMode);
  }

  useEffect(() => {
    const theme = darkMode ? "dark" : "light";
    document.documentElement.setAttribute("data-theme", theme);

    localStorage.setItem(
      "prefers-dark-theme",
      JSON.stringify(darkMode ? true : false)
    );
  }, [darkMode]);

  return (
    <label className={`${className} cursor-pointer grid place-items-center`}>
      <input
        type="checkbox"
        value="synthwave"
        className="toggle theme-controller bg-base-content row-start-1 col-start-1 col-span-2"
        onClick={toggleTheme}
        defaultChecked={darkMode || false}
      />
      <ThemeToggleSunIco />
      <ThemeToggleMoonIco />
    </label>
  );
}
